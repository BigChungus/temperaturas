package com.example.misegundaaplicacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {




    private TextView tvClimaChillan;
    private TextView tvClimaMoscu;
    private TextView tvClimaParis;
    private TextView tvClimaMadrid;
    private TextView tvHumedadChillan;
    private TextView tvHumedadParis;
    private TextView tvHumedadMadrid;
    private TextView tvHumedadMoscu;
    private TextView tvPrecionChillan;
    private TextView tvPrecionMoscu;
    private TextView tvPrecionMadrid;
    private TextView tvPrecionParis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvClimaChillan = (TextView) findViewById(R.id.tvClimaChillan);
        this.tvClimaParis = (TextView) findViewById(R.id.tvClimaParis);
        this.tvClimaMadrid = (TextView) findViewById(R.id.tvClimaMadrid);
        this.tvClimaMoscu = (TextView) findViewById(R.id.tvClimaMoscu);
        this.tvPrecionParis = (TextView) findViewById(R.id.tvPrecionParis);
        this.tvPrecionMadrid = (TextView) findViewById(R.id.tvPrecionMadrid);
        this.tvPrecionMoscu = (TextView) findViewById(R.id.tvPrecionMoscu);
        this.tvPrecionChillan = (TextView) findViewById(R.id.tvPrecionChillan);
        this.tvHumedadChillan = (TextView) findViewById(R.id.tvHumedadChillan);
        this.tvHumedadMadrid = (TextView) findViewById(R.id.tvHumedadMadrid);
        this.tvHumedadMoscu = (TextView) findViewById(R.id.tvHumedadMoscu);
        this.tvHumedadParis = (TextView) findViewById(R.id.tvHumedadParis);

        String url = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6067&lon=-72.1034&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";
        String url2 = "http://api.openweathermap.org/data/2.5/weather?lat=55.7522202&lon=37.6155586&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";
        String url3 = "http://api.openweathermap.org/data/2.5/weather?lat=40.4167&lon=-3.70325&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";
        String url4 = "http://api.openweathermap.org/data/2.5/weather?lat=48.8667&lon=2.333335&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";

        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humedad = mainJSON.getDouble("humidity");
                            double precion = mainJSON.getDouble("pressure");
                            tvClimaChillan.setText("Temperatura = " + temp + "°c");
                            tvHumedadChillan.setText("Humedad = " + humedad );
                            tvPrecionChillan.setText("Preción = " + precion + " HPA" );

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        StringRequest solicitud2 = new StringRequest(
                Request.Method.GET,
                url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humedad = mainJSON.getDouble("humidity");
                            double precion = mainJSON.getDouble("pressure");
                            tvClimaMoscu.setText("Temperatura = " + temp + "°c");
                            tvHumedadMoscu.setText("Humedad = " + humedad );
                            tvPrecionMoscu.setText("Preción = " + precion + " HPA" );

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        StringRequest solicitud3 = new StringRequest(
                Request.Method.GET,
                url3,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humedad = mainJSON.getDouble("humidity");
                            double precion = mainJSON.getDouble("pressure");
                            tvClimaMadrid.setText("Temperatura = " + temp + "°c");
                            tvHumedadMadrid.setText("Humedad = " + humedad );
                            tvPrecionMadrid.setText("Preción = " + precion + " HPA" );

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        StringRequest solicitud4 = new StringRequest(
                Request.Method.GET,
                url4,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humedad = mainJSON.getDouble("humidity");
                            double precion = mainJSON.getDouble("pressure");
                            tvClimaParis.setText("Temperatura = " + temp + "°c");
                            tvHumedadParis.setText("Humedad = " + humedad );
                            tvPrecionParis.setText("Preción = " + precion + " HPA" );

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);
        RequestQueue listaEspera2 = Volley.newRequestQueue(getApplicationContext());
        listaEspera2.add(solicitud2);
        RequestQueue listaEspera3 = Volley.newRequestQueue(getApplicationContext());
        listaEspera3.add(solicitud3);
        RequestQueue listaEspera4 = Volley.newRequestQueue(getApplicationContext());
        listaEspera4.add(solicitud4);
    }
}
